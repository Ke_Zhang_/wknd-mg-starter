import React, { Component } from 'react';
import { Provider } from "react-redux";
import { StyleSheet, Text, View } from 'react-native';
import { Expo, Font, AppLoading } from "expo";
import configStore from "./src/config/configStore";
import RootStack from './src/navigator';


// async function getCameraRollAsync() {
//     const { Location, Permissions } = Expo;
//     const { status } = await Permissions.askAsync(Expo.Permissions.CAMERA_ROLL);
//     if (status !== "granted") {
//         throw new Error("Camera Roll permission not granted");
//     }
// }

class App extends Component {
    state = {
        fontsAreLoaded: false
    };

    // async componentWillMount() {

    //     getCameraRollAsync();
    // }

    // componentDidMount() {
    //     Amplify.configure(awsmobile);
    // }

    render() {
        // const store = createStore(reducers, {}, applyMiddleware(reduxThunk));
        const store = configStore();

        // if (!this.state.fontsAreLoaded) {
        //     return <AppLoading />;
        // }

        return (
            <Provider store={store}>
                <RootStack />
            </Provider>
        );
    }
}

export default App;
