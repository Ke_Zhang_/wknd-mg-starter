import { combineReducers } from "redux";

const loginReducer = (state = {}, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

const AuthenticationReducer = combineReducers({
    loginReducer
});

export default AuthenticationReducer;