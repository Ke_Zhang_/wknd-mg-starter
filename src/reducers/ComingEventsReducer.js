import { combineReducers } from "redux";
import {
    GET_EVENTS_REQUEST_ACTION
} from "../actions/actionTypes/ComingEventsActionTypes";

import { MOCK_DATA_PAGE_1 } from '../mocks';

const ComingEventsReducer = (state = {comingEvents:[]}, action) => {
    switch (action.type) {
        case GET_EVENTS_REQUEST_ACTION:
            return {
                ...state,
                comingEvents: MOCK_DATA_PAGE_1
            }
        default:
            return state;
    }
};

// const EventReducers = combineReducers({
//     ComingEventsReducer
// });

export default ComingEventsReducer;