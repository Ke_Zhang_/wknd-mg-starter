import { combineReducers } from "redux";

const eventsReducer = (state = {}, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

const EventReducers = combineReducers({
    eventsReducer
});

export default EventReducers;