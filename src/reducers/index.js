import { combineReducers } from "redux";
import AuthenticationReducer from './AuthenticationReducer';
import EventReducers from './EventsReducer';
import ComingEventsReducer from './ComingEventsReducer';

export default combineReducers({
    AuthenticationReducer,
    EventReducers,
    ComingEventsReducer
});