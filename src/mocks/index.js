export const MOCK_DATA_PAGE_4 = [
  {
    attendee: [
      {
        amount: 500,
        customer: "cus_D8ccCHnYElxXpM",
        sourceId: "card_1CiLMzCSBkQY8ge0VmWYD6IJ",
        status: "yes",
        username: "censihan"
      }
    ],
    booking: {
      bookingId: "9e4c9200-b231-11e8-bb90-db6f4094c8b7",
      description: "Booth for the night",
      name: "My Booth",
      type: "BOOTH"
    },
    datetime: 1536325628739,
    event: {
      coverPhoto: "https://marketplace.canva.com/MACqP-EkKeU/1/0/thumbnail_large/canva-violet-adult-halloween-party-facebook-event-cover-MACqP-EkKeU.jpg",
      dateTime: "1543748444000",
      endDatetime: "1543809644000",
      eventManager: "test",
      eventSnapshotId: "e5593d20-b0ff-11e8-a1dd-e5d905d43f08",
      location: "Toronto",
      title: "Pawty Time",
      venue: "1150 Queen St W, Toronto, ON M6J 1J3, Canada"
    },
    recipient: "test",
    requestId: "ed1ba930-b29e-11e8-b583-37f4423d56da",
    sender: "censihan",
    status: "waiting for payment",
    type: "booth_request"
  },
  {
    attendee: [
      {
        amount: 500,
        customer: "cus_D8ccCHnYElxXpM",
        sourceId: "card_1CiLMzCSBkQY8ge0VmWYD6IJ",
        status: "yes",
        username: "censihan"
      }
    ],
    booking: {
      bookingId: "9e4c9200-b231-11e8-bb90-db6f4094c8b7",
      description: "Booth for the night",
      name: "My Booth",
      type: "BOOTH"
    },
    datetime: 1536325628739,
    event: {
      coverPhoto: "test+055FF4AD-6A46-4EA9-B8B0-76959A14FCE3.jpg",
      dateTime: 1543752044000,
      endDatetime: 1543755644000,
      eventManager: "test",
      eventSnapshotId: "554d3640-b100-11e8-a1dd-e5d905d43f07",
      location: "Toronto",
      title: "Diplo (US)",
      venue: "11 Polson St, Toronto, ON M5A 1A4, Canada"
    },
    recipient: "test",
    requestId: "ed1ba930-b29e-11e8-b583-37f4423d56da",
    sender: "censihan",
    status: "waiting for payment",
    type: "booth_request"
  },
  {
    attendee: [
      {
        amount: 500,
        customer: "cus_D8ccCHnYElxXpM",
        sourceId: "card_1CiLMzCSBkQY8ge0VmWYD6IJ",
        status: "yes",
        username: "censihan"
      }
    ],
    booking: {
      bookingId: "9e4c9200-b231-11e8-bb90-db6f4094c8b7",
      description: "Booth for the night",
      name: "My Booth",
      type: "BOOTH"
    },
    datetime: 1536325628739,
    event: {
      coverPhoto: "test+1B56D9F5-8CBC-4FEA-97E0-34D7658F87E9.jpg",
      dateTime: "1543748444000",
      endDatetime: "1543809644000",
      eventManager: "test",
      eventSnapshotId: "e5593d20-b0ff-11e8-a1dd-e5d905d43f07",
      location: "Toronto",
      title: "Pawty Time",
      venue: "1150 Queen St W, Toronto, ON M6J 1J3, Canada"
    },
    recipient: "test",
    requestId: "ed1ba930-b29e-11e8-b583-37f4423d56da",
    sender: "censihan",
    status: "waiting for payment",
    type: "booth_request"
  }
];

export const MOCK_DATA_PAGE_1 = [
  {
    booth: [],
    coverPhoto: 'https://marketplace.canva.com/MACqP-EkKeU/1/0/thumbnail_large/canva-violet-adult-halloween-party-facebook-event-cover-MACqP-EkKeU.jpg',
    //coverPhoto: "./mockImages/1.jpg",
    dateTime: "1544330971000",
    description: "w/ TRIPSIXX & DRIPXXXX",
    dress: "Hypebeast",
    endDatetime: 1544100321000,
    eventId: "f0ed2730-751f-11e8-9f25-1795eb89f248",
    eventManager: "test",
    eventSnapshotId: "a07117d0-b101-11e8-a1dd-e5d905d43f07",
    genre: ["pop", "R&B", "Soul"],
    geoLocation: {
      lat: 43.6647275,
      lng: -79.3741718
    },
    guestList: [],
    location: "Toronto",
    published: false,
    ticket: [],
    title: "SKI MASK THE SLUMP GOD",
    venue: "410 Sherbourne St, Toronto, ON M5X 1K2, Canada",
    venueName: "The Phoenix"
  },
  {
    booth: [
      {
        bookingId: "f4ee9c20-b231-11e8-bb90-db6f4094c8b7"
      },
      {
        bookingId: "86b4e8b0-e03d-11e8-b75c-3db53297b00e"
      }
    ],
    //coverPhoto: "test+055FF4AD-6A46-4EA9-B8B0-76959A14FCE3.jpg",
    coverPhoto: 'https://marketplace.canva.com/MACqLahwmS0/1/0/thumbnail_large/canva-sunset-party-photo-facebook-event-cover-MACqLahwmS0.jpg',
    dateTime: "1544417371000",
    description: "Cabana Pool Lounge Presents",
    dress: "Casual",
    endDatetime: "1543755644000",
    eventId: "f0ed2730-751f-11e8-9f25-1795eb89f249",
    eventManager: "test",
    eventSnapshotId: "554d3640-b100-11e8-a1dd-e5d905d43f07",
    genre: ["pop", "DJ"],
    geoLocation: {
      lat: 43.64086229999999,
      lng: -79.35456409999999
    },
    guestList: [
      {
        bookingId: "df90a3a0-b231-11e8-bb90-db6f4094c8b7"
      }
    ],
    location: "Toronto",
    published: false,
    ticket: [
      {
        bookingId: "c2442e70-b231-11e8-bb90-db6f4094c8b7"
      },
      {
        bookingId: "6cb49140-e03d-11e8-b75c-3db53297b00e"
      }
    ],
    title: "Diplo (US)",
    venue: "11 Polson St, Toronto, ON M5A 1A4, Canada",
    venueName: "Cabana Pool Lounge"
  },
  {
    booth: [],
    coverPhoto: 'https://marketplace.canva.com/MACqQGnNpNg/1/0/thumbnail_large/canva-orange-fall-festival-facebook-event-cover-MACqQGnNpNg.jpg',
    //coverPhoto: "./mockImages/1.jpg",
    dateTime: "1545281371000",
    description: "w/ TRIPSIXX & DRIPXXXX",
    dress: "Hypebeast",
    endDatetime: 1544100321000,
    eventId: "f0ed2730-751f-11e8-9f25-1795eb89f240",
    eventManager: "test",
    eventSnapshotId: "a07117d0-b101-11e8-a1dd-e5d905d43f07",
    genre: ["pop", "R&B", "Soul"],
    geoLocation: {
      lat: 43.6647275,
      lng: -79.3741718
    },
    guestList: [],
    location: "Toronto",
    published: false,
    ticket: [],
    title: "SKI MASK THE SLUMP GOD",
    venue: "410 Sherbourne St, Toronto, ON M5X 1K2, Canada",
    venueName: "The Phoenix"
  },
  {
    booth: [],
    coverPhoto: 'https://marketplace.canva.com/MACqKTHJM2Y/1/0/thumbnail_large/canva-midsummer-music-festival-facebook-event-cover-MACqKTHJM2Y.jpg',
    //coverPhoto: "./mockImages/1.jpg",
    dateTime: "1545281371000",
    description: "w/ TRIPSIXX & DRIPXXXX",
    dress: "Hypebeast",
    endDatetime: 1544100321000,
    eventId: "f0ed2730-751f-11e8-9f25-1795eb89f241",
    eventManager: "test",
    eventSnapshotId: "a07117d0-b101-11e8-a1dd-e5d905d43f07",
    genre: ["pop", "R&B", "Soul"],
    geoLocation: {
      lat: 43.6647275,
      lng: -79.3741718
    },
    guestList: [],
    location: "Toronto",
    published: false,
    ticket: [],
    title: "SKI MASK THE SLUMP GOD",
    venue: "410 Sherbourne St, Toronto, ON M5X 1K2, Canada",
    venueName: "The Phoenix"
  },
  {
    booth: [],
    coverPhoto: 'https://marketplace.canva.com/MACqQUs9R2k/1/0/thumbnail_large/canva-yellow-island-photo-going-away-party-facebook-event-cover-MACqQUs9R2k.jpg',
    //coverPhoto: "./mockImages/1.jpg",
    dateTime: "1545281371000",
    description: "w/ TRIPSIXX & DRIPXXXX",
    dress: "Hypebeast",
    endDatetime: 1544100321000,
    eventId: "f0ed2730-751f-11e8-9f25-1795eb89f242",
    eventManager: "test",
    eventSnapshotId: "a07117d0-b101-11e8-a1dd-e5d905d43f07",
    genre: ["pop", "R&B", "Soul"],
    geoLocation: {
      lat: 43.6647275,
      lng: -79.3741718
    },
    guestList: [],
    location: "Toronto",
    published: false,
    ticket: [],
    title: "SKI MASK THE SLUMP GOD",
    venue: "410 Sherbourne St, Toronto, ON M5X 1K2, Canada",
    venueName: "The Phoenix"
  },
];
