import React, { PureComponent } from "react";
import { TouchableOpacity, Text, View, Image } from 'react-native';
import Styles from "../components/EventsComponents/styles";
import appStyles from "../utils/appStyles.js";

import { DrawerActions, createStackNavigator, createDrawerNavigator, createMaterialTopTabNavigator, createAppContainer } from "react-navigation";
import authViewContainer from '../containers/AuthenticationContainer';
//import mainViewContainer from '../containers/EventsContainer';
import followerViewContainer from '../containers/FollowersContainer';
//import ComingEvents from "../components/EventsComponents/CommingEvents";
//import ComingEventsContainer from "../containers/CommingEventsContainer";
import ComingEventsContainer from "../containers/ComingEventsContainer";
import PastEventsContainer from "../containers/PastEventsContainer";

import CustomDrawer from './CustomDrawer';
const { screen, themeColor, fonts } = appStyles;
const eventTabs = createMaterialTopTabNavigator(
    {
        Upcoming: ComingEventsContainer,
        Past: PastEventsContainer
    },
    {
        tabBarOptions: {
            activeTintColor: "#fff",
            inactiveTintColor: "gray",
            style: {
                backgroundColor: "#2B292E"
            },
            indicatorStyle: {
                backgroundColor: "#fff"
            },
            tabStyle: {
                width: 120,
            },
            upperCaseLabel:false,
        },
        navigationOptions: ({ navigation }) => ({
            title: 'ReactNavigation',  // Title to appear in status bar
            headerLeft:
            <View style={Styles.customHeader}>
                <TouchableOpacity onPress={() => { navigation.dispatch(DrawerActions.toggleDrawer()) }}>
                    <Image
                        source={require("../assets/icons/menu_options.png")}
                        resizeMode="center"
                    />
                </TouchableOpacity>
                <Image
                    source={require("../assets/images/WKND_white.png")}
                    resizeMode="center"
                    style={{ width: 100, height: 50 }}
                />
                {/* <View style={{width:30,height:30,backgroundColor:'red'}}>
                </View> */}
                <TouchableOpacity style={{ paddingBottom: 15, paddingRight: 15 }}>
                    <Image
                        source={require("../assets/images/gold_ovals.png")}
                        resizeMode="center"
                        style={{ width: 25, height: 25 }}
                    />
                </TouchableOpacity>
            </View>,
            headerStyle: {
                backgroundColor: '#222224',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },

        })
    }
);

const eventStack = createStackNavigator(
    {
        EventsBoth: {
            screen: eventTabs,
        },
    },
);

const mainDrawer = createDrawerNavigator({
    Events: {
        screen: eventStack
    },
    Follower: {
        screen: followerViewContainer
    }
},
    {
        initialRouteName: 'Events',
        contentComponent: CustomDrawer,
        drawerWidth:screen.width * 0.7
    }
);

const rootStack = createStackNavigator(
    {
        authViewContainer: {
            screen: authViewContainer
        },
        mainViewContainer: {
            screen: mainDrawer
        }
    },
    {
        initialRouteName: "authViewContainer",
        headerMode: 'none'
    }
);

export default createAppContainer(rootStack)