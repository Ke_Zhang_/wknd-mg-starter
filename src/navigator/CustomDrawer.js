import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';
import { Text, View, StyleSheet, Image } from 'react-native';
import appStyles from "../utils/appStyles.js";

const { screen, themeColor, fonts } = appStyles;
export default class CustomDrawer extends Component {

    navigateToScreen = ( route ) =>(
        () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
    })

  render() {
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
                <View style={{paddingRight:15}}>
                    <Image source={require('../assets/icons/profile-placeholder.png')} style={{width: 50, height: 50}}/>
                </View>
                <View style={{justifyContent:'space-between'}}>
                    <Text style={styles.headerText}>
                        @alanallday
                    </Text>
                    <Text style={styles.subHeaderText}>
                        455 events
                    </Text>
                </View>
            </View>
            <View style={styles.screenContainer}>
                <View style={styles.screenStyle}>
                    <Text style={styles.menuText} onPress={this.navigateToScreen('Events')}>Events</Text>
                </View>
                <View style={styles.screenStyle}>
                    <Text style={styles.menuText} onPress={this.navigateToScreen('Follower')}>Follower</Text>
                </View>
                <View style={styles.screenStyle}>
                    <Text style={styles.menuText} onPress={this.navigateToScreen('Events')}>Promote</Text>
                </View>
                <View style={styles.screenStyle}>
                    <Text style={styles.menuText} onPress={this.navigateToScreen('Follower')}>Analytics</Text>
                </View>
                <View style={styles.screenStyle}>
                    <Text style={styles.menuText} onPress={this.navigateToScreen('Follower')}>Settings</Text>
                </View>
            </View> 
        </View>
    
    )
  }
}

const styles = StyleSheet.create({
    container: {
        //alignItems: 'center',
        backgroundColor: themeColor.black,
        height:screen.height,
        padding:30,
        paddingTop:50
    },
    headerContainer: {
        height: 150,
        backgroundColor:'#97805F'
    },
    headerText: {
        ...fonts.M2,
        color: themeColor.white, 
    },
    subHeaderText: {
        ...fonts.S,
        color: themeColor.gray, 
    },
    menuText: {
        ...fonts.XL,
        color: themeColor.white, 
    },
    screenContainer: {
        paddingTop: 30
    },
    screenStyle: {
        height: 30,
        marginTop: 23,
        flexDirection: 'row',
        alignItems: 'center'
    },
    screenTextStyle:{
        fontSize: 20,
        marginLeft: 20
    },

});