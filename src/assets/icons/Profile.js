import React from "react";
import PropTypes from "prop-types";
import Svg, {
    G,
    Path,
    Circle,
    LinearGradient,
    Defs,
    Use,
    Stop
} from "react-native-svg";

const UnselectedProfile = () => (
    <Svg width="40" height="40" viewBox="0 0 40 40">
        <G
            id="Page-1"
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd"
        >
            <G id="User2">
                <Circle id="Oval" cx="18" cy="18" r="17" stroke="#bebebe" />
                <G
                    id="user"
                    transform="translate(10.000000, 8.000000)"
                    stroke="#bebebe"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                >
                    <Path d="M16,18 L16,16 C16,13.790861 14.209139,12 12,12 L4,12 C1.790861,12 0,13.790861 0,16 L0,18" />
                    <Circle id="Oval" cx="8" cy="4" r="4" />
                </G>
            </G>
        </G>
    </Svg>
);

const SelectedProfile = () => (
    <Svg width="40" height="40" viewBox="0 0 40 40">
        <Defs>
            <LinearGradient
                x1="0.442557803%"
                y1="50%"
                x2="100%"
                y2="50%"
                id="linearGradient-1"
            >
                <Stop stopColor="#907A58" offset="0%" />
                <Stop stopColor="#C09F75" offset="100%" />
            </LinearGradient>
        </Defs>
        <G
            id="Page-1"
            stroke="none"
            stroke-width="1"
            fill="none"
            fill-rule="evenodd"
        >
            <G id="User2">
                <Circle
                    id="Oval"
                    fill="url(#linearGradient-1)"
                    cx="18"
                    cy="18"
                    r="17"
                />
                <G
                    id="user"
                    transform="translate(10.000000, 8.000000)"
                    stroke="#FFFFFF"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                >
                    <Path d="M16,18 L16,16 C16,13.790861 14.209139,12 12,12 L4,12 C1.790861,12 0,13.790861 0,16 L0,18" />
                    <Circle id="Oval" cx="8" cy="4" r="4" />
                </G>
            </G>
        </G>
    </Svg>
);

const ProfileIcon = ({ selected }) =>
    selected ? <SelectedProfile /> : <UnselectedProfile />;

ProfileIcon.propTypes = {
    selected: PropTypes.bool
};
ProfileIcon.defaultProps = {
    selected: false
};

export default ProfileIcon;
