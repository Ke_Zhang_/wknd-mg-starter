import React, { Component } from "react";
import { FileSystem } from "expo";
import Svg, { Defs, Image, ClipPath, G, Path, Rect } from "react-native-svg";

class TicketShape extends Component {
    state = {
        uri: null
    };

    async componentDidMount() {
        const res = await FileSystem.downloadAsync(
            this.props.source,
            `${FileSystem.documentDirectory}coverPhoto-${Date.now()}.jpeg`
        );

        this.setState({ uri: res.uri });
    }

    render() {
        const { uri } = this.state;
        const { width, height } = this.props;
        return (
            uri && (
                <Svg
                    width={width}
                    height={height}
                    viewBox={`0 0 ${width} ${height}`}
                >
                    <Defs>
                        <ClipPath id="ticket-mask">
                            <G stroke="#fff" strokeWidth="1" fill="#fff">
                                <Path
                                    d={`M${width},${height - 8} L${width -
                                        6},${height - 8} C${width -
                                        8.71644},${height - 8} ${width -
                                        11},${height - 5.7614237} ${width -
                                        11},${height - 3} L${width -
                                        11},${height} L11,${height} L11,${height -
                                        3} C11,${height -
                                        5.7614237} 8.76142375,${height -
                                        8} 6,${height - 8} L0,${height -
                                        8} L0,7 L6,7 C8.76142375,7 11,4.76142375 11,2 L11,0 L${width -
                                        11},0 L${width - 11},2 C${width -
                                        11},4.76142375 ${width -
                                        8.71644},7 ${width -
                                        6},7 L${width},7 L${width},${height -
                                        8} Z`}
                                />
                            </G>
                        </ClipPath>
                    </Defs>
                    <Image
                        x="0%"
                        y="0%"
                        width="100%"
                        height="100%"
                        preserveAspectRatio="xMidYMid slice"
                        href={{ uri }}
                        clipPath="url(#ticket-mask)"
                    />

                    <Image
                        x="0"
                        y="0"
                        width="100%"
                        height="100%"
                        preserveAspectRatio="xMidYMid slice"
                        href={require("assets/images/TicketBackground.png")}
                        clipPath="url(#ticket-mask)"
                        opacity="0.6"
                    />
                    {this.props.children}
                </Svg>
            )
        );
    }
}

export default TicketShape;
