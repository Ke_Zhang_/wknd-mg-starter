
const DAYS_IN_A_WEEK = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
];

const MONTHS = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec"
];


export const getEventTimeString = event => {
    const { dateTime, endDatetime } = event;
    const startTime = new Date(Number(dateTime));
    const endTime = new Date(Number(endDatetime));

    const startTimeString = startTime.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit"
    });
    const endTimeString = endTime.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit"
    });

    const dateInfoString = `${
        DAYS_IN_A_WEEK[startTime.getDay()]
    } ${startTime.getDate()} ${MONTHS[startTime.getMonth()]}`;

    return {
        startTimeString,
        endTimeString,
        dateInfoString
    };
};
