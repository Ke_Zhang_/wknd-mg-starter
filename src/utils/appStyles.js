import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
const screen = { width, height };
const themeColor = {
    orange: "#F7941D", // this is theme color from Appster
    color: "#f15a29", // this is theme color from heard agency
    darkGreen: "#00968a",
    black: "#000",
    blackIntroTitle: "#222222",
    blackIntroText: "#414042",
    white: "#fff",
    whiteSmoke: "#efefef",
    gray: "#aaa", // disabled text, place holder, all other gray
    greySepratorLine: "#979797", //separator line gray
    greyishText: "#848484", //sub-text gray
    graybackground: "#d8dee7",
    header: "#222224"
};
const defaultFont = {
    color: themeColor.black
    //fontFamily: "CiutadellaRounded-Regular"
};

const fonts = {
    XXL: {
        ...defaultFont,
        fontSize: 31
    },
    XL2: {
        ...defaultFont,
        fontSize: 28
    },
    XL: {
        ...defaultFont,
        fontSize: 25
    },
    L: {
        ...defaultFont,
        fontSize: 22
    },
    M2: {
        ...defaultFont,
        fontSize: 18
    },
    M: {
        ...defaultFont,
        fontSize: 16
    },
    S: {
        ...defaultFont,
        fontSize: 13
    },
    S1: {
        ...defaultFont,
        fontSize: 14
    },
    XS: {
        ...defaultFont,
        fontSize: 12
    },
    XS1: {
        ...defaultFont,
        fontSize: 10
    },
    bold: {
        ...defaultFont,
        fontWeight: "bold"
    },
    underline: {
        textDecorationLine: "underline"
    },
    italic: {
        ...defaultFont,
        fontStyle: "italic"
    }
};
const navigatorStyle = {
    // any custom navigator style will sit here
};

export default {
    navigatorStyle,
    fonts,
    screen,
    themeColor
};
