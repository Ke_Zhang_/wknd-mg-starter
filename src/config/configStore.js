import { createStore, applyMiddleware, compose } from "redux";
import logger from "redux-logger";

import rootReducer from '../reducers';

export default function configStore(initialState = {}) {
    const store = createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(logger))
    )

    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = rootReducer;
            store.replaceReducer(nextRootReducer);
        });
    }
    return store;
}