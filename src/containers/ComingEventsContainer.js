import React, { PureComponent } from "react";

import {
    View,
    Text,
    StatusBar,
    Image,
    Button,
    SafeAreaView,
    FlatList,
    SectionList,
    Modal,
    TouchableHighlight,
    TouchableOpacity
} from "react-native";

import PropTypes from "prop-types";

import Styles from "../components/EventsComponents//styles";
import { SavedEventSquare } from '../components/EventsComponents/SavedEventSquare';
import { SectionHeader } from '../components/EventsComponents/SectionHeader';
import { SectionFooter } from '../components/EventsComponents/SectionFooter';
import { MOCK_DATA_PAGE_1 } from '../mocks';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import CreateEventModal from '../components/EventsComponents/CreateEventModal';

import { connectEnhance } from "../config/enhance";
import { bindActionCreators } from "redux";
import * as ComingEventsActions from "../actions/ComingEventsActions";

class ComingEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        }
        this.setModalVisible = this.setModalVisible.bind(this);
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    componentWillMount() {
        this.props.GetEventsAction();
    }

    render() {
        // if(!this.props.events){
        //     return 
        // }
        let today = [];
        let tomorrow = [];
        let upcoming = [];
        for (let i = 0; i < this.props.events.length; i++) {
            if (moment(new Date(Number(this.props.events.dateTime))).isSame(moment(), 'day')) {
                // today
                today.push(this.props.events[i]);
            } else if (moment(new Date(Number(this.props.events[i].dateTime))).isSame(moment(new Date()).add(1, 'days'), 'day')) {
                tomorrow.push(this.props.events[i]);
            } else {
                upcoming.push(this.props.events[i]);
            }
        }
        let sections = [
            { key: 'Tonight', data: today },
            { key: 'Tomorrow', data: tomorrow },
            { key: 'Upcoming', data: upcoming }
        ];

        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#2B292E",
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <StatusBar barStyle="light-content" />
                <SectionList
                    renderSectionHeader={(section) => <SectionHeader event={section} />}
                    renderItem={({ item }) => <SavedEventSquare event={item} />}
                    sections={sections}
                    keyExtractor={item => item.eventId}
                    stickySectionHeadersEnabled={false}
                />
                <ActionButton buttonColor="#97805F" onPress={() => { this.setModalVisible(true) }} position="right" />
                <CreateEventModal 
                    modalVisible={this.state.modalVisible}
                    setModalVisible={this.setModalVisible}
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        events: state.ComingEventsReducer.comingEvents
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ComingEventsActions }, dispatch);
}

export default connectEnhance(
    ComingEvents,
    mapStateToProps,
    mapDispatchToProps
);
