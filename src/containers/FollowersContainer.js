import React from "react";
import { connectEnhance } from "../config/enhance";
import { bindActionCreators } from "redux";
import * as EventActions from "../actions/AuthenticationActions";

import Follower from "../components/FollowersComponents";


const followerViewContainer = ({ ...props }) => <Follower {...props} />;


function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...EventActions }, dispatch);
}

export default connectEnhance(
    followerViewContainer,
    mapStateToProps,
    mapDispatchToProps
);
