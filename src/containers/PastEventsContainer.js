import React, { PureComponent } from "react";

import {
    View,
    Text,
    StatusBar,
    Image,
    Button,
    SafeAreaView,
    FlatList
} from "react-native";

import PropTypes from "prop-types";

import Styles from "../components/EventsComponents/styles";
import PastEventSquare from '../components/EventsComponents/PastEventSquare';
import { PastEventsListHeader } from '../components/EventsComponents/PastEventsListHeader';
import { MOCK_DATA_PAGE_4 } from '../mocks';

import { connectEnhance } from "../config/enhance";
import { bindActionCreators } from "redux";
import * as ComingEventsActions from "../actions/ComingEventsActions";

class PastEvents extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#2B292E",
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <FlatList
                    ListHeaderComponent={PastEventsListHeader}
                    data={MOCK_DATA_PAGE_4}
                    renderItem={({ item }) => <PastEventSquare event={item} />}
                    keyExtractor={item => item.event.eventSnapshotId}
                />

            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ComingEventsActions }, dispatch);
}

export default connectEnhance(
    PastEvents,
    mapStateToProps,
    mapDispatchToProps
);
