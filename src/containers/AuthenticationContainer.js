import React from "react";
import { connectEnhance } from "../config/enhance";
import { bindActionCreators } from "redux";
import * as AuthActions from "../actions/AuthenticationActions";

import AuthView from "../components/AuthenticationComponents";

const authViewContainer = ({ ...props }) => <AuthView {...props} />;

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...AuthActions }, dispatch);
}

export default connectEnhance(
    authViewContainer,
    mapStateToProps,
    mapDispatchToProps
);
