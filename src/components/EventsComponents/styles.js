import { StyleSheet } from "react-native";
import appStyles from "../../utils/appStyles.js";

const { screen, themeColor, fonts } = appStyles;
// const threeImgWidth = (SCREEN.width - 52) / 3;
// const twoImgWidth = (SCREEN.width - 40) / 2;
const Styles = StyleSheet.create({
    customHeader: {
        flexDirection: "row",
        height: 45,
        width: screen.width,
        backgroundColor: themeColor.header,
        justifyContent: "space-between",
        alignItems: "flex-end"
    },
    customHeaderInModal: {
        flexDirection: "row",
        height: 80,
        width: screen.width,
        backgroundColor: themeColor.header,
        justifyContent: "space-between",
        alignItems: "flex-end"
    },
    footerAddNew: {
        width: screen.width * 0.9,
        height: 40,
        backgroundColor: '#97805F',
        position:'absolute',
        alignSelf:'flex-end'
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    // below is copy from InviteFeed.styles.js
    feedWrapper: {
        width: screen.width * 0.9,
        height: 100,
        marginHorizontal: 16,
        paddingBottom: 15,
        marginBottom: 15
    },
    inviter: {
        color: "#fff",
        fontFamily: "Barlow-Regular",
        fontSize: 16,
        lineHeight: 19
    },
    attendee: {
        color: "#fff",
        fontFamily: "Barlow-SemiBold",
        fontWeight: "600",
        fontSize: 16,
        lineHeight: 19,
        textDecorationLine: "underline"
    },
    feed: {
        borderRadius: 6,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 20,
        shadowColor: "rgb(23,21,27)",
        shadowOpacity: 0.79,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        minHeight: 81,
        marginTop: 17
    },
    eventImage: {
        minHeight: 100,
        minWidth:100
    },
    imageWrapper: {
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
        height: "100%"
    },
    eventDetail: {
        marginLeft: 17,
        justifyContent: "space-between",
        flex: 1
    },
    eventTitle: {
        color: "#fff",
        fontWeight: "600",
        //fontFamily: "Barlow-Medium",
        fontSize: 16,
        alignSelf: "flex-start",
        lineHeight: 19
    },
    eventVenue: {
        color: "#B9BFC7",
        //fontFamily: "Barlow-Regular",
        fontSize: 14,
        lineHeight: 20
    },
    eventDateWrapper: {
        borderColor: "rgba(151,151,151,0.3)",
        borderWidth: 1,
        borderRadius: 4,
        paddingTop: 6,
        paddingHorizontal: 11,
        paddingBottom: 11,
        marginRight: 16,
        alignItems: "center"
    },
    dateOfWeek: {
        color: "#C09F75",
        fontSize: 12,
        lineHeight: 18,
        //fontFamily: "Barlow-SemiBold",
        fontWeight: "bold"
    },
    date: {
        color: "#fff",
        fontSize: 12,
        lineHeight: 18,
        //fontFamily: "Barlow-Regular"
    },
    btnActionsWrapper: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    // buttonStyle: {
    //     button: {
    //         minHeight: 40,

    //         borderRadius: 5,
    //         shadowOffset: {
    //             width: 0,
    //             height: 2
    //         },
    //         shadowRadius: 10,
    //         shadowColor: "rgb(187,155,114)",
    //         shadowOpacity: 0.29
    //     },
    //     buttonWrapper: {
    //         padding: 0,
    //         marginTop: 16,
    //         flex: 0.48
    //     },
    //     buttonText: {
    //         fontSize: 16,
    //         lineHeight: 19,
    //         letterSpacing: 0.5
    //     }
    // }

});

export default Styles;