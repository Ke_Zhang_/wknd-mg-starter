import React, { PureComponent } from "react";

import {
    View,
    Text,
    StatusBar,
    Image,
    Button,
    SafeAreaView
} from "react-native";
import { LinearGradient } from "expo";

import PropTypes from "prop-types";

import Styles from "./styles";
import { getEventTimeString } from "../../utils/time";

export default class PastEventSquare extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

    const { event } = this.props;
    const { dateInfoString } = getEventTimeString(event.event);
    const arr = dateInfoString.split(" ");
    const dateOfWeek = arr[0].substr(0, 3).toUpperCase();
    const date = [arr[2], arr[1]].join(" ");

        return (
        <View style={Styles.feedWrapper}>
        <LinearGradient
          colors={["#202020", "#414145"]}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          style={Styles.feed}
        >
          <View style={Styles.imageWrapper} overflow="hidden">
            <Image
              source={{ uri: event.event.coverPhoto }}
              style={Styles.eventImage}
              resizeMode="cover"
            />
          </View>

          <View style={Styles.eventDetail}>
            <Text style={Styles.eventTitle}>{event.event.title}</Text>
            <Text style={Styles.eventVenue}>{event.event.income}</Text>
          </View>
          <View style={Styles.eventDateWrapper}>
            <Text style={Styles.dateOfWeek}>{dateOfWeek}</Text>
            <Text style={Styles.date}>{date}</Text>
          </View>
        </LinearGradient>
      </View>
        );
    }
}
