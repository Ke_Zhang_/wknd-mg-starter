import React from "react";
//import { ImageBackground, View, Text, TouchableOpacity } from "@shoutem/ui";
import { ImageBackground, View, Text, TouchableOpacity } from "react-native";
//import { Actions } from "react-native-router-flux";
import { LinearGradient } from "expo";
import appStyles from "../../utils/appStyles.js";
//import { COVER_PHOTO_FOLDER } from "constants/const";
const { screen, themeColor, fonts } = appStyles;
export const SectionHeader = ({ event }) => {

    let weekday = '';
    let monthDay  = '';
    let display = false;
    if (event.section.key === 'Tonight') {
        const date = new Date();
         weekDay = date.toLocaleString(undefined, { weekday: "short" });
         monthDay = date.toLocaleString(undefined, {
            month: "short",
            day: "numeric"
        });
        display = true
    } else if (event.section.key === 'Tomorrow') {
        let date = new Date();
        let tomorrow = new Date();
        tomorrow.setDate(date.getDate()+1);
         weekDay = tomorrow.toLocaleString(undefined, { weekday: "short" });
         monthDay = tomorrow.toLocaleString(undefined, {
            month: "short",
            day: "numeric"
        });
        display = true
    }

    return (
        <View style={{ paddingTop: 30, justifyContent:'center' }}>
            <Text style={{ color: 'white', fontSize: 20 }}>
                {event.section.key}
            </Text>
            {display &&
            <Text style={{ color: '#d8dee7', fontSize: 13,paddingTop:3 }}>
                {weekDay + ',' + monthDay}
            </Text>
            }
        </View>
    );
};


