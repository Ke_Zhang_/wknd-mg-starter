import React from "react";
//import { ImageBackground, View, Text, TouchableOpacity } from "@shoutem/ui";
import { View, Text, TouchableOpacity, Image } from "react-native";
//import { Actions } from "react-native-router-flux";
import appStyles from "../../utils/appStyles.js";
import { Ionicons } from "@expo/vector-icons";
//import { COVER_PHOTO_FOLDER } from "constants/const";
const { screen, themeColor, fonts } = appStyles;
export default CreateEventOptions = (props) => {

    return (
        <View style={styles.container}>
            <TouchableOpacity style={{ width: screen.width, borderBottomWidth: 0.5, borderBottomColor: themeColor.blackIntroText, display: 'flex', flexDirection: 'row', padding: 25 }}>
                <View style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', width: screen.width * 0.15 }}>
                    <Image
                        source={require("../../assets/icons/eventCreation.png")}
                        resizeMode="center"
                        style={{ width: 20, height: 20 }}
                    />
                </View>
                <View style={{ width: screen.width * 0.65 }}>
                    <Text style={{ ...fonts.M, color: 'white' }}>
                        {props.title}
                        </Text>
                    <Text style={{ ...fonts.S, color: themeColor.graybackground, paddingTop: 10 }}>
                        {props.text}
                        </Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = {
    container: {
        backgroundColor: "#2B292E",
        alignItems: "center"
    }
};
