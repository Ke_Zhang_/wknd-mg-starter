import React from "react";
//import { ImageBackground, View, Text, TouchableOpacity } from "@shoutem/ui";
import { ImageBackground, View, Text, TouchableOpacity, TextInput } from "react-native";
//import { Actions } from "react-native-router-flux";
import appStyles from "../../utils/appStyles.js";
import { Ionicons } from "@expo/vector-icons";
//import { COVER_PHOTO_FOLDER } from "constants/const";
const { screen, themeColor, fonts } = appStyles;
export const PastEventsListHeader = () => {

    return (
    <View style={styles.searchViewStyle}>
        <TouchableOpacity>
            <Ionicons name="ios-search" color="#B9BFC7" size={30} />
        </TouchableOpacity>
        <TextInput
            style={styles.inputStyle}
            placeholder='Search 16 events...'
            autoCorrect={false}
            //onChangeText={onChangeText}
            //value={value}
            autoCapitalize="none"
            placeholderTextColor='#B9BFC7'
        />
    </View>
    );
};

const styles = {
    inputStyle: {
        flex: 1,
        fontSize: 20,
        paddingLeft:10
    },
    searchViewStyle: {
        backgroundColor: "#222224",
        paddingLeft: 15,
        paddingRight: 15,
        marginLeft: 15,
        marginRight: 15,
        height:50,
        marginTop: 15,
        // marginBottom: 10,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: "#4B4B4B",
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
        
    }
};
