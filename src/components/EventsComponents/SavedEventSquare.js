import React from "react";
//import { ImageBackground, View, Text, TouchableOpacity } from "@shoutem/ui";
import { ImageBackground, View, Text, TouchableOpacity } from "react-native";
//import { Actions } from "react-native-router-flux";
import { LinearGradient } from "expo";
import appStyles from "../../utils/appStyles.js";
//import { COVER_PHOTO_FOLDER } from "constants/const";
const { screen, themeColor, fonts } = appStyles;
export const SavedEventSquare = ({ event }) => {
  const {
    squareStyle,
    imageBackgroundStyle,
    infoViewStyle,
    textStyle1,
    textStyle2,
    textStyle3,
    textStyle4,
    leftTextBoxStyle,
    rightTextBoxStyle
  } = styles;

  const date = new Date(Number(event.dateTime));
  const weekDay = date.toLocaleString(undefined, { weekday: "short" });
  const monthDay = date.toLocaleString(undefined, {
    month: "short",
    day: "numeric"
  });
  //const coverPhoto = COVER_PHOTO_FOLDER + event.coverPhoto.replace("+", "%2B");
  const coverPhoto = event.coverPhoto;
  return (
      <TouchableOpacity
        style={squareStyle}
      >
        <ImageBackground
          source={{ uri: coverPhoto }}
          //source = {require(`${coverPhoto}`)}
          style={imageBackgroundStyle}
        >
          <View style={{ flex: 1 }} />
          <LinearGradient
            colors={["transparent", "black"]}
            style={{ flex: 1, width: screen.width * 0.9 }}
          />
          <View style={infoViewStyle}>
            <View style={leftTextBoxStyle}>
              <Text style={textStyle1}>{weekDay.toUpperCase()}</Text>
              <Text style={textStyle2}>{monthDay}</Text>
            </View>
            <View style={rightTextBoxStyle}>
              <Text style={textStyle3}>{event.title}</Text>
              <Text style={textStyle4}>{event.location}</Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
  );
};

const styles = {
  squareStyle: {
    borderRadius: 15,
    overflow: "hidden",
    marginTop:20,
    //justifyContent:'center',
    //alignItems:'center'
    //marginRight: 15
  },
  imageBackgroundStyle: {
    height: screen.width * 0.78,
    width: screen.width * 0.9,
    flexDirection: "column"
  },
  infoViewStyle: {
    flexDirection: "row",
    backgroundColor: "black"
  },
  textStyle1: {
    color: "#C09F75",
    //fontFamily: "Barlow-Medium",
    fontSize: 12
  },
  textStyle2: {
    color: "white",
    //fontFamily: "Barlow-Medium",
    fontSize: 12
  },
  textStyle3: {
    color: "white",
    fontWeight: "600",
    //fontFamily: "Barlow-SemiBold",
    lineHeight: 18,
    fontSize: 15
  },
  textStyle4: {
    color: "#B9BFC7",
    //fontFamily: "Barlow-Medium",
    fontSize: 15,
    lineHeight: 18
  },
  leftTextBoxStyle: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderRightColor: "rgba(185,191,199,0.17)",
    borderRightWidth: 1
  },
  rightTextBoxStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    padding: 10
  }
};
