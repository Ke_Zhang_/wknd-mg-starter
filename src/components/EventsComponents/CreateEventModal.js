import React from "react";
import { ImageBackground, View, Text, TouchableOpacity, Modal, Image } from "react-native";
import appStyles from "../../utils/appStyles.js";
import CreateEventOptions from './CreateEventOptions';
import PastEventSquare from './PastEventSquare';

//mock
import { MOCK_DATA_PAGE_4 } from '../../mocks';

const { screen, themeColor, fonts } = appStyles;

export default CreateEventModal = (props) => {

    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={props.modalVisible}
            onRequestClose={() => {
                Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.customHeaderInModal}>
                <View style={{ width: 20 }}>
                </View>
                <Image
                    source={require("../../assets/images/WKND_white.png")}
                    resizeMode="center"
                    style={{ width: 100, height: 50 }}
                />
                <TouchableOpacity onPress={() => props.setModalVisible(false)} style={{ paddingBottom: 15, paddingRight: 15 }}>
                    <Image
                        source={require("../../assets/icons/closeIcon.png")}
                        resizeMode="center"
                        style={{ width: 15, height: 15 }}
                    />
                </TouchableOpacity>
            </View>
            <View style={{backgroundColor: "#2B292E",height:screen.height}}>
                <CreateEventOptions 
                    title='Create new event'
                    text="Start from scratch. This process won't take long."
                />
                <CreateEventOptions 
                    title='Copy from past event'
                    text="Take a past event on WKND and use it as a template to create a new one."
                />
                <View style={{padding:15}}>
                    <Text style={{ ...fonts.M2, color: 'white' }}>
                        Drafts
                    </Text>
                    <Text style={{ ...fonts.S1, color: themeColor.graybackground,paddingTop:5 }}>
                        Continue from where you left off.
                    </Text>
                </View>
                <PastEventSquare event={MOCK_DATA_PAGE_4[0]}/>
            </View>
        </Modal>
    );
};

const styles = {
    customHeaderInModal: {
        flexDirection: "row",
        height: 80,
        width: screen.width,
        backgroundColor: themeColor.header,
        justifyContent: "space-between",
        alignItems: "flex-end"
    },
    container: {
        backgroundColor: "#2B292E",
        alignItems: "center"
    }
};
