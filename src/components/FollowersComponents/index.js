import React, { PureComponent } from "react";

import {
    View,
    Text,
    StatusBar,
    Image,
    Button,
    SafeAreaView
} from "react-native";

import PropTypes from "prop-types";

import Styles from "./styles";
import CustomHeader from './customHeader';

export default class Follower extends React.Component {

    constructor(props) {
        super(props);
    }

    toggleDrawer = () => {
        this.props.navigation.openDrawer();
    };

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#2B292E",
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <StatusBar barStyle="light-content" />
                <CustomHeader toggleDrawer={this.toggleDrawer}/>
                <View 
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <Text style={{ color: "white" }}>Follower</Text>
                </View>
            </View>
        );
    }
}