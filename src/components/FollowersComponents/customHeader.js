import React, { PureComponent } from "react";

import {
    View,
    Text,
    StatusBar,
    Image,
    Button,
    TouchableOpacity
} from "react-native";
import Styles from "./styles";

export default class CustomHeader extends React.Component {
    render() {
        const { toggleDrawer } = this.props;
        return (
            <View style={Styles.customHeader}>
                <TouchableOpacity onPress={toggleDrawer}>
                    <Image
                        source={require("../../assets/icons/menu_options.png")}
                        resizeMode="center"
                    />
                </TouchableOpacity>
                <Image
                    source={require("../../assets/images/WKND_white.png")}
                    resizeMode="center"
                    style={{ width: 100, height: 50 }}
                />
                {/* <View style={{width:30,height:30,backgroundColor:'red'}}>
                </View> */}
                <TouchableOpacity style={{paddingBottom:15,paddingRight:15}}>
                    <Image
                        source={require("../../assets/images/gold_ovals.png")}
                        resizeMode="center"
                        style={{ width: 25, height: 25 }}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}
