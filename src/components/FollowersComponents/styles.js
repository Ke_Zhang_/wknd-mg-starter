import { StyleSheet } from "react-native";
import appStyles from "../../utils/appStyles.js";
const { screen, themeColor, fonts } = appStyles;
// const { SCREEN, THEME_COLOR, fonts } = COMMON_STYLE;
// const threeImgWidth = (SCREEN.width - 52) / 3;
// const twoImgWidth = (SCREEN.width - 40) / 2;
const Styles = StyleSheet.create({
    customHeader: {
        flexDirection: "row",
        height: 86,
        width: screen.width,
        backgroundColor: themeColor.header,
        justifyContent: "space-between",
        alignItems: "flex-end"
    }
});

export default Styles;