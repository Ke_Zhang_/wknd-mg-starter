import * as actionTypes from './actionTypes/AuthenticationActionTypes';

const someActionsRequest = request => ({
    type: actionTypes.SOME_ACTIONS_REQUEST,
    payload: request
});

const someActionsResponse = request => ({
    type: actionTypes.SOME_ACTIONS_RESPONSE,
    payload: request
});


export  {
    someActionsRequest,
    someActionsResponse,
};