import * as actionTypes from './actionTypes/ComingEventsActionTypes';

const GetEventsAction = request => ({
    type: actionTypes.GET_EVENTS_REQUEST_ACTION,
    payload: request
});

export  {
    GetEventsAction
};